function SinhVien(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa) {
  this.ma = maSv;
  this.ten = tenSv;
  this.email = email;
  this.matKhau = matKhau;
  this.diemToan = diemToan;
  this.diemLy = diemLy;
  this.diemHoa = diemHoa;
  this.tinhDTB = function () {
    return (this.diemToan * 1 + this.diemHoa * 1 + this.diemLy * 1) / 3;
  };
}
