function layThongTinTuFrom() {
    var maSv = document.getElementById("txtMaSV").value.trim();
    var tenSv = document.getElementById("txtTenSV").value.trim();
    var email = document.getElementById("txtEmail").value.trim();
    var matKhau = document.getElementById("txtPass").value.trim();
    var diemToan = document.getElementById("txtDiemToan").value;
    var diemLy = document.getElementById("txtDiemLy").value;
    var diemHoa = document.getElementById("txtDiemHoa").value;
  
    var sv = new SinhVien(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa);
    console.log("sv: ", sv);
    return sv;
  }
  function renderDanhSachSinhVien(list) {
    var contentHTML = "";
  
    list.forEach(function (item) {
      var content = `<tr>
          <td>${item.ma}</td>
          <td>${item.ten}</td>
          <td>${item.email}</td>
          <td>${item.tinhDTB()}</td>
          <td>
              <button class="btn btn-danger" onclick="xoaSV('${
                item.ma
              }')" >Xoá</button>
              <button class="btn btn-warning" onclick="suaSV('${
                item.ma
              }')" >Sửa</button>
          </td>
      </tr>`;
      contentHTML += content;
    });
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
  }
  
  function timKiemViTri(id, arr) {
    for (var index = 0; index < arr.length; index++) {
      var sv = arr[index];
      if (sv.ma == id) {
        return index;
      }
    }
    return -1;
  }
  function showThongTinLenForm(sv) {
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtDiemToan").value = sv.diemToan;
    document.getElementById("txtDiemLy").value = sv.diemLy;
    document.getElementById("txtDiemHoa").value = sv.diemHoa;
  }
  