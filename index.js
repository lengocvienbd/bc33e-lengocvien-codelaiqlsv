var dssv = [];
const DSSV = "DSSV";
// lấy dssv từ localstorage dưới dạng json
var dssvLocalStorage = localStorage.getItem(DSSV);

if (JSON.parse(dssvLocalStorage)) {
  var data = JSON.parse(dssvLocalStorage);
  console.log("before", data);
  for (var index = 0; index < data.length; index++) {
    debugger;
    var current = data[index];
    var sv = new SinhVien(
      current.ma,
      current.ten,
      current.email,
      current.matKhau,
      current.diemToan,
      current.diemLy,
      current.diemHoa
    );
    dssv.push(sv);
    console.log("after:", dssv);
  }

  renderDanhSachSinhVien(dssv);

  function saveLocalStorage() {
    // conver array thành json
    var dssvJson = JSON.stringify(dssv);
    console.log("dssvJson: ", dssvJson);
    // lưu xuống local storage
    localStorage.setItem(DSSV, dssvJson);
  }
}
// thêm sv
function themSV() {
  var newSV = layThongTinTuFrom();
  dssv.push(newSV);
  saveLocalStorage();
  // xuất danh sách
  renderDanhSachSinhVien(dssv);

  renderDanhSachSinhVien(dssv);
}

// xoá sv
function xoaSV(id) {
  var index = timKiemViTri(id, dssv);
  if (index !== -1) {
    dssv.splice(index, 1);
    saveLocalStorage();
    renderDanhSachSinhVien(dssv);
  }
}

// sửa sv
function suaSV(id) {
  var index = timKiemViTri(id, dssv);
  // console.log("index", index);
  if (index !== -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}
// cập nhập sv
function capNhapSv() {
  var svEdited = layThongTinTuFrom();
  console.log("svEdited: ", svEdited);
  let index = timKiemViTri(svEdited.ma, dssv);

  if (index != -1) {
    dssv[index] = svEdited;
    saveLocalStorage();
    renderDanhSachSinhVien(dssv);
  }
  showThongTinLenForm(sv);
}
